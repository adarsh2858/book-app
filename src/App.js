import React, { Suspense, useEffect, useState } from "react";
import logo from "./logo.svg";
import starRating from "./assets/rating.png";
import axios from "axios";
import "./App.css";

const BookCard = ({ book }) => {
  let starWidthPercent = ( book.average_rating / 5.0 ) * 100;
  starWidthPercent = starWidthPercent.toFixed(2);
  return (
    <div className="p-4 bg-gray-500 shadow-md m-4 rounded-lg">
      <span>{book.bookID}</span>
      <h1>Title: {book.title}</h1>
      <h4>Price: {book.price}</h4>
      <div>Percent - {starWidthPercent}</div>
      <span>
        Average Rating: {book.average_rating}
        <div className="progress" style={
          {width: `18%`}
        }>
          <img className="filler" width="72" src={starRating} alt="rating" />
        </div>
      </span>
      <span>ISBN: {book.isbn}</span>
    </div>
  );
};

const sortArray = (type, books, setBooks) => {
  const types = {
    default: "bookID",
    rating: "average_rating",
  };

  const sortProperty = types[type];
  const sorted = books.sort((a, b) => b[sortProperty] - a[sortProperty]);
  setBooks(sorted);
};

const Search = ({ bookFilterOnChange, className }) => {
  return (
    <div className={className}>
      <label htmlFor="search">Search by name:</label>
      <input
        className="text-black rounded-md ml-2 px-2 py-1"
        type="text"
        onChange={(event) => {
          const prevEvent = event;
          setTimeout(() => bookFilterOnChange(prevEvent), 2000);
        }}
      />
    </div>
  );
};

function App() {
  const url =
    "https://s3-ap-southeast-1.amazonaws.com/he-public-data/books8f8fe52.json";
  const [books, setBooks] = useState([]);
  const [sortType, setSortType] = useState([]);
  const [inputValue, setInputValue] = useState("");

  const bookFilterOnChange = (event) => {
    setInputValue(event.target.value);
  };

  useEffect(() => {
    getAllBooks();
  }, []);

  useEffect(() => {}, [sortType, inputValue]);

  const getAllBooks = async () => {
    try {
      const { data } = await axios.get(url);

      setBooks(data);
    } catch (err) {
      alert(err);
    }
  };

  const filteredBooks = books.filter((book) => {
    return book.title
      ?.toString()
      .toLowerCase()
      .includes(inputValue.toLowerCase());
  });

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        {books && (
          <Search
            className="m-4"
            allBooks={books}
            inputValue={inputValue}
            bookFilterOnChange={bookFilterOnChange}
          />
        )}
        {books && (
          <div>
            Sort By -
            <select
              className="text-black"
              onChange={(e) => {
                setSortType(e.target.value);
                sortArray(e.target.value, books, setBooks);
              }}
            >
              <option value="default">Book ID</option>
              <option value="rating">Average Rating</option>
            </select>
          </div>
        )}
        {books && (
          <div className="grid gap-4 grid-cols-1 sm:grid-cols-2 md:grid-cols-3">
            <Suspense fallback={<h1>Loading Books...</h1>}>
              {inputValue.length > 0
                ? filteredBooks.map((book) => (
                    <BookCard key={book.bookID} book={book} />
                  ))
                : books.map((book) => (
                    <BookCard key={book.bookID} book={book} />
                  ))}
            </Suspense>
          </div>
        )}
      </header>
    </div>
  );
}

export default App;
