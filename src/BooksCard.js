import React from "react";
import starRating from "./assets/rating.png";

const BooksCard = ({ books }) => (
  books.map((book) => 
     (
      <div key={book.bookID} className="p-4 bg-blue-100 m-4 rounded-lg">
        <h1>{book.title}</h1>
        <h4>{book.price}</h4>
        <span className="progress">
          {book.average_rating}
          <img width="72" src={starRating} alt="rating" />
        </span>
        <span>{book.isbn}</span>
      </div>
    )
  )
);

export default <BooksCard />;
